# Utilizamos una imagen de node como base
FROM node:latest

# Establecemos el directorio de trabajo en el contenedor
WORKDIR /app

# Copiamos los archivos de la aplicación al directorio de trabajo
COPY ./root/package.json ./root/package-lock.json ./

# Instalamos las dependencias del proyecto
RUN npm install

# Copiamos el resto de los archivos de la aplicación al directorio de trabajo
COPY ./root/ ./

# Exponemos el puerto 443 (el puerto por defecto de la aplicación React)
EXPOSE 443

# Comando para iniciar la aplicación cuando se ejecute el contenedor
CMD ["npm", "start"]


# docker build -t root_imagen .
# docker stop root_contenedor
# docker rm root_contenedor
# docker run -p 443:443 --name root_contenedor root_imagen
# mf1-react-carga
# body1 npm run build